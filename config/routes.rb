Rails.application.routes.draw do
  resources :verifications
  resources :watchdogs
  resources :users
  root "users#homepage"
  get "confirmmail" => "users#confirmmail"
  get "unlockaccount" => "users#unlockaccount"
  get "two_factor" => "users#get_two_factor"
  post "two_factor" => "users#post_two_factor"
  get "user_log" => "watchdogs#user_log"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
