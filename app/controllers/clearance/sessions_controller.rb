class Clearance::SessionsController < Clearance::BaseController
    if respond_to?(:before_action)
      before_action :redirect_signed_in_users, only: [:new]
      skip_before_action :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_action :authorize,
        only: [:create, :new, :destroy],
        raise: false
    else
      before_filter :redirect_signed_in_users, only: [:new]
      skip_before_filter :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_filter :authorize,
        only: [:create, :new, :destroy],
        raise: false
    end
    #before_action :get_two_factor ,:except => [:new , :create	,:destroy	]
    
    
  
    def create
      @user = authenticate(params)
      @token = SecureRandom.hex(4)

       if @user and @user.status == 0 # prevent user from logging untill confirming his email
        flash.now.notice = 'You should confirm your email first'
        render template: "sessions/new"

       elsif @user and @user.disabled == 1 # prevent user fron logging if his account is locked
          flash.now.notice = 'Your account is locked,please visit your email to activate it.'
          render template: "sessions/new"

       #elsif @user and @user.failed_attempts > 3

       else
  
           sign_in(@user) do |status|
        
                if status.success?
                  @user.loginattempts += 1
                  @user.failedattempts = 0
                  @user.save
                  session[:token_code] = @token
                   # get last signin to check if current ip is different from last ip send email to user
                  @last_ip = Watchdog.where("admin_id =? ", @user.id ).last
                  @current_ip = request.env['REMOTE_ADDR']
                  @db = MaxMindDB.new('./GeoLite2-Country.mmdb')
                  @last_ip_country = @db.lookup(@last_ip.ipaddress)                  
                  @current_ip_country = @db.lookup(@current_ip) 
                  if @last_ip_country.country.name != @current_ip_country.country.name
                    UserMailer.differentip(@user,@current_ip).deliver_later
                  end 
                  Watchdog.create(:admin_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now  )
                  UserMailer.confirmsignin(@user,@token).deliver_later
                  redirect_to two_factor_path
                  #redirect_back_or url_after_create
                else
                  @currentuser = User.where("email =?",params[:session][:email].to_s).first
                  if @currentuser != nil 
                    @currentuser.failedattempts += 1
                     if @currentuser.failedattempts >= 3
                       @currentuser.disabled = 1
                       UserMailer.unlockaccount(@currentuser,@token).deliver_later
                     end
                     @currentuser.save
                  end
                flash.now.notice = status.failure_message
                render template: "sessions/new", status: :unauthorized
                end
           end
       end
    end
  
    def destroy
      sign_out
      redirect_to url_after_destroy
    end
  
    def new
      render template: "sessions/new"
    end
  
    private
  
    def redirect_signed_in_users
      if signed_in?
        redirect_to url_for_signed_in_users
      end
    end
  
    def url_after_create
      Clearance.configuration.redirect_url
    end
  
    def url_after_destroy
      sign_in_url
    end
  
    def url_for_signed_in_users
      url_after_create
    end
  end