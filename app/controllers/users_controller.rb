class UsersController < ApplicationController
  before_action :require_login , only: [:show, :edit, :update, :destroy,:index]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :get_two_factor ,only: [:show, :edit, :update,:index]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  def homepage  
  
  end

  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    @user.failedattempts = 0
    @user.disabled = 0
    @user.save
    flash.now.notice = 'Your account is Unlocked,please sign in .'
    render template: "sessions/new"
    

  end

  def confirmmail
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    @verify_code = Verification.where("admin_id =?",@id ).first
    if @verify_code.email_confirmation_token == @token
      @user.update(:status => 1)
      @verify_code.update(:email_confirmed_at => Time.now)      
    end
    redirect_to @user
  end

  # GET /users/new
  def new
    @user = User.new
    render :layout => false
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.uuid = SecureRandom.hex(4)
    @token = SecureRandom.hex(4)
    respond_to do |format|
      if @user.save
        Verification.create(:admin_id => @user.id,:email_confirmation_token => @token)
        Watchdog.create(:admin_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now )
        UserMailer.confirmmail(@user,@token).deliver_later
        format.html { redirect_to sign_in_path	, notice: 'User was successfully created,You should confirm your email before sign in' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  

  def get_two_factor
  end

  def post_two_factor
    @user_code = params[:confirm][:code]
    @sent_code = session[:token_code]
    session[:verify_code] = false
    if @user_code == @sent_code
      flash.now.notice = 'Welcome back!'
      session[:verify_code] = true

      redirect_to users_path
    else 
      flash.now.notice = 'wrong code!'
      redirect_to two_factor_path
    end
    
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:uuid, :roleid,  :firstname, :lastname, :language, :disabled, :loginattempts, :username, :email, :password)
      
    end
end
