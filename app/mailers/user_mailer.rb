class UserMailer < ApplicationMailer
    default from: "Payers <no-reply@payers.net>"
     
     def differentip(user,ip)
       
       @email = user.email
       @ip = ip   
             
         mail to: user.email, subject: 'Welcome to My Awesome Site'
     end

     def confirmmail(user,token)
       
      @email = user.email
      @token = token   
      @id = user.id
            
        mail to: user.email, subject: 'Please Confirm your account'
    end

    def confirmsignin(user,token)
       
        @email = user.email
        @token = token   
        @id = user.id
              
          mail to: user.email, subject: 'Please Confirm your account'
      end

    def unlockaccount(user,token)
       
        @email = user.email
        @token = token   
        @id = user.id
              
          mail to: user.email, subject: 'Your account has been locked'
      end

      
end
