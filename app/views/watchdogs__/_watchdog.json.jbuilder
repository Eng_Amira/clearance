json.extract! watchdog, :id, :user_id, :ip, :details, :created_at, :updated_at
json.url watchdog_url(watchdog, format: :json)
