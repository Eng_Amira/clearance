class User < ApplicationRecord
  include Clearance::User
  has_many :watchdog , foreign_key: "admin_id"

  def self.setagent(useragentt)
    $useragenttx = useragentt.env['HTTP_USER_AGENT']
    $ip =   useragentt.remote_ip
  end

end
